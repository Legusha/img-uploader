'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  AUTH_URL: '"https://api.imgur.com/oauth2/authorize"',
  ROOT_API: '"https://api.imgur.com/3/account"',
  ALBUMS_COVER: '"https://i.imgur.com/"',
  UPLOAD_IMG_URL: '"https://api.imgur.com/3/image"'
})
