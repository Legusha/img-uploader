'use strict'
module.exports = {
  NODE_ENV: '"production"',
  AUTH_URL: '"https://api.imgur.com/oauth2/authorize"',
  ROOT_API: '"https://api.imgur.com/3/account"',
  ALBUMS_COVER: '"https://i.imgur.com/"',
  UPLOAD_IMG_URL: '"https://api.imgur.com/3/image"'
}
