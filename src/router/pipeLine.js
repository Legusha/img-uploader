/**
 * This method serves to recursively call middleware.
 * @param {Object} args Parameters to be passed to middleware
 * @param {Array} middleware All middleware that must be called
 * @param {Number} index Index to search middleware
 * @return {Function}
 */

function pipeLine (args, middleware, index) {
  const nextMiddleware = middleware[index]
  if (!nextMiddleware) {
    return args.next
  }
  return () => {
    const nextPipeline = pipeLine(args, middleware, ++index)
    nextMiddleware({ ...args }, nextPipeline)
  }
}
export default pipeLine
