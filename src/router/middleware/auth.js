import { getItemFromLocalStorage } from '@api/jwt.service'

export default function ({ next }, nextMiddleware) {
  if (getItemFromLocalStorage('access_token') == null) {
    next({name: 'singIn'})
  }
  return nextMiddleware()
}
