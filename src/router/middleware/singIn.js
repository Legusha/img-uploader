import { getItemFromLocalStorage, setLocalStorage, parseParamsAuth } from '@api/jwt.service'
import api from '@api'

export const parseParams = function ({ next }, nextMiddleware) {
  const strHash = window.location.hash
  if (strHash.length > 0) {
    const params = parseParamsAuth(strHash)
    setLocalStorage(params)
    api.setAuthHeader(params.token)
  }
  nextMiddleware()
}

export const checkAuth = function ({ next }, nextMiddleware) {
  if (getItemFromLocalStorage('access_token') !== null) {
    next({name: 'gallery'})
  }
  nextMiddleware()
}
