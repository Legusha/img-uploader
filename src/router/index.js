import Vue from 'vue'
import Router from 'vue-router'
import gallery from '@/page/gallery'
import singIn from '@/page/singIn'
import error404 from '@/page/error404'
import albums from '@/components/gallery/albums'
import uploader from '@/components/gallery/uploader'
import albumsList from '@/components/gallery/albums/list'
import imageList from '@/components/gallery/albums/imageList'
import imageView from '@/components/gallery/albums/imageView'

import pipeLine from './pipeLine'
import auth from './middleware/auth'
import { checkAuth, parseParams } from './middleware/singIn'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'gallery',
      redirect: {name: 'albums'},
      component: gallery,
      children: [
        {
          path: 'albums',
          name: 'albums',
          redirect: { name: 'albumsList' },
          component: albums,
          children: [
            {
              path: 'list',
              name: 'albumsList',
              component: albumsList,
              meta: { middleware: [auth] }
            },
            {
              path: 'image-list/:id',
              name: 'imageList',
              component: imageList,
              meta: { middleware: [auth] }
            },
            {
              path: 'view/:id',
              name: 'imageView',
              component: imageView,
              meta: { middleware: [auth] }
            }
          ]
        },
        {
          path: 'upload',
          name: 'uploader',
          component: uploader,
          meta: { middleware: [auth] }
        }
      ]
    },
    {
      path: '/singIn',
      name: 'singIn',
      component: singIn,
      meta: { middleware: [parseParams, checkAuth] }
    },
    {
      path: '*',
      name: 'error',
      component: error404
    }
  ]
})
/**
 * Router navigation guards which checks and calls if the path to the component has middleware
 */

router.beforeEach((to, from, next) => {
  if ('middleware' in to.meta) {
    const middleware = to.meta.middleware
    const startMiddlewareIndex = 0
    const args = {
      to,
      from,
      next
    }
    return middleware[startMiddlewareIndex](
      {...args},
      pipeLine(args, middleware, startMiddlewareIndex + 1)
    )
  }
  next()
})
export default router
