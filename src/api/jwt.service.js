/**
 * Get data from local storage
 * @param {String} key
 * @return {string}
 */
export const getItemFromLocalStorage = (key) => {
  return window.localStorage.getItem(key)
}
/**
 * Remove data from local storage
 * @param {String} itemKey
 * @return {void}
 */
export const removeItemFromLocalStorage = (itemKey) => {
  window.localStorage.removeItem(itemKey)
}
/**
 * Set data from local storage
 * @param {String} key
 * @param {String} value
 * @return {void}
 */
export const setItemToLocalStorage = (key, value) => {
  window.localStorage.setItem(key, value)
}
/**
 * Set data to local storage from Object
 * @param {Object} params
 * @return {void}
 */
export const setLocalStorage = (params) => {
  for (let key in params) {
    if (key in params) {
      setItemToLocalStorage(key, params[key])
    }
  }
}
/**
 * Parse params from hash string
 * @param {String} hash
 * @return {Object}
 */
export const parseParamsAuth = (hash) => {
  if (hash.length === 0) {
    return {}
  }
  return hash
    .replace('#', '')
    .split('&')
    .reduce((prev, item) => {
      return Object.assign({[item.split('=')[0]]: item.split('=')[1]}, prev)
    }, {})
}
/**
 * This method is called to redirect Imgur authentication.
 * @param {String} clientId
 * @return {void}
 */
export const singIn = (clientId) => {
  const responseType = 'token'
  if (clientId.length === 0) {
    return
  }
  window.location.href = `${process.env.AUTH_URL}?client_id=${clientId}&response_type=${responseType}`
}
export default {
  getItemFromLocalStorage,
  removeItemFromLocalStorage,
  setItemToLocalStorage,
  parseParamsAuth,
  setLocalStorage,
  singIn
}
