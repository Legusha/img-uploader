import axios from 'axios'
import router from '@/router'
import { getItemFromLocalStorage, removeItemFromLocalStorage } from './jwt.service'

/**
 * This service is intended for interaction with API.
 * @example await api.get('/albums');
 * @param {Object} config Axios defaults option to override
 * @param {Object} xhr Axios methods which do you want to use
 * @return {void}
 */

const xhr = {
  get: axios.get,
  post: axios.post
}

class Api {
  constructor (config, xhr) {
    if (typeof Api.instance === 'object') {
      return Api.instance
    }
    this.config = config
    this.xhr = xhr
    this.typeImgFormat = 'base64'
    this.setAuthHeader()
    Api.instance = this
    return this
  }
  setAuthHeader (token = getItemFromLocalStorage('access_token')) {
    this.config.headers.common['Authorization'] = `Bearer ${token}`
  }
  errorAuthHandler (e) {
    if (e.response.status === 403 || e.response.status === 401) {
      removeItemFromLocalStorage('access_token')
      router.push({name: 'singIn'})
    }
  }
  async get (query) {
    const userName = getItemFromLocalStorage('account_username')
    try {
      const { data } = await this.xhr.get(`${process.env.ROOT_API}/${userName}${query}`)
      return data
    } catch (e) {
      console.error(e)
      this.errorAuthHandler(e)
    }
  }
  async uploadImage (body) {
    body.type = this.typeImgFormat
    try {
      const { data } = await this.xhr.post(`${process.env.UPLOAD_IMG_URL}`, body)
      return data
    } catch (e) {
      console.error(e)
      this.errorAuthHandler(e)
    }
  }
}
export default new Api(axios.defaults, xhr)
